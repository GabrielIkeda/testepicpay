<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => '/users'], function () use ($router) {
    $router->get('/', 'UserController@list');
    $router->get('/{id}', 'UserController@get');
    $router->post('/', 'UserController@create');
    $router->post('/consumers', 'ConsumerController@create');
    $router->post('/sellers', 'SellerController@create');
});

$router->group(['prefix' => '/transactions'], function () use ($router) {
    $router->post('/', 'TransactionController@create');
    $router->get('/{id}', 'TransactionController@get');
});

$router->post('/validation', 'TransactionController@validation');