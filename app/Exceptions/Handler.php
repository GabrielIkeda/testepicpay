<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use PDOException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        NotFoundHttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof PDOException) {
            return response()->json([
               'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
               'message' => 'Erro interno do servidor'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => 'Registro nao encontrado'
            ],Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof HttpException){
            if ($exception->getCode() === Response::HTTP_NOT_FOUND) {
                return response()->json([
                    'code' => Response::HTTP_NOT_FOUND,
                    'message' => 'Usuário nao encontrado'
                ], Response::HTTP_NOT_FOUND);
            }
        }

        if ($exception instanceof ValidationException) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => 'Erro de validacao nos campos'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return parent::render($request, $exception);
    }
}
