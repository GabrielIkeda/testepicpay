<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Collection;

class UserRepository
{

    private $model;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getAll()
    {
        return User::query()->orderBy('full_name')->get();
    }

    public function getWithRelations($id)
    {
        return $this->model::with('account.seller')
            ->with('account.consumer')
            ->where('id', $id)
            ->first();
    }

    /**
     * @param $parameter
     * @return Collection
     */
    public function getByNameOrUsername($parameter)
    {
        return $this->model::query()->select('users.*')
            ->leftJoin('sellers AS s', 's.user_id', '=', 'users.id')
            ->leftJoin('consumers AS c', 'c.user_id', '=', 'users.id')
            ->whereRaw("users.full_name LIKE '$parameter%'")
            ->orWhereRaw("c.username LIKE '$parameter%'")
            ->orWhereRaw("s.username RLIKE '$parameter%'")
            ->get();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

}