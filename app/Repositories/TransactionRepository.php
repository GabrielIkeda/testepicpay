<?php

namespace App\Repositories;

use App\Models\Transaction;
use Carbon\Carbon;

class TransactionRepository
{
    private $model;

    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }

    public function create(array $parameters)
    {
        $parameters['transaction_date'] = Carbon::now('america/sao_paulo')->format('Y-m-d\TH:i:s.u');

        return $this->model->create($parameters);
    }

}