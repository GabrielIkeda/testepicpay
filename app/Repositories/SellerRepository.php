<?php

namespace App\Repositories;

use App\Models\Seller;

class SellerRepository
{
    private $model;

    /**
     * ServiceRepository constructor.
     * @param Seller $seller
     */
    public function __construct(Seller $seller)
    {
        $this->model = $seller;
    }

    /**
     * @param array $parameters
     * @return Seller
     */
    public function create(array $parameters):Seller
    {
        return $this->model->create($parameters);
    }
}