<?php

namespace App\Repositories;

use App\Models\Consumer;

class ConsumerRepository
{
    private $model;

    /**
     * @param Consumer $consumer
     */
    public function __construct(Consumer $consumer)
    {
        $this->model = $consumer;
    }

    /**
     * @param array $data
     * @return Consumer
     */
    public function create(array $data):Consumer
    {
        return $this->model->create($data);
    }
}