<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConsumerResource;
use App\Services\ConsumerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{

    private $service;

    public function __construct(ConsumerService $consumerService)
    {
        $this->service = $consumerService;
    }

    /**
     * @param Request $request
     * @return ConsumerResource
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'bail|required|unique:consumers|integer',
            'username' => 'bail|required|unique:consumers|unique:sellers'
        ]);

        $consumer = $this->service->create($request->all());

        return new ConsumerResource($consumer);
    }

}