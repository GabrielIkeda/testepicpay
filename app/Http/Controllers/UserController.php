<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use \Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws \Exception
     */
    public function list(Request $request)
    {
        $response = $this->userService->list($request->all());

        if ($response === null) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return UserResource::collection($response);
    }

    /**
     * @param Request $request
     * @return UserResource
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'bail|required|string',
            'cpf' => 'bail|required|size:11|unique:users',
            'email' => 'bail|required|email|unique:users',
            'phone_number' => 'bail|required|numeric',
            'password' => 'bail|required'
        ]);

        $response = $this->userService->create($request->all());

        return new UserResource($response);
    }

    /**
     * @param Request $request
     * @param $userId
     * @return UserResource
     * @throws \Exception
     */
    public function get(Request $request, $userId)
    {
        $response = $this->userService->get($userId);

        if ($response === null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return new UserResource($response);
    }

}