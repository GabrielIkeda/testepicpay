<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionResource;
use App\Services\TransactionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $service;

    /**
     * @param TransactionService $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->service = $transactionService;
    }

    /**
     * @param Request $request
     * @return TransactionResource
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'payee_id' => 'bail|required|exists:accounts,id',
            'payer_id' => 'bail|required|exists:accounts,id',
            'value' => 'bail|required|integer|min:1'
        ]);

        $transaction = $this->service->create($request->all());

        return new TransactionResource($transaction);
    }

    /**
     * @param Request $request
     * @param $transactionId
     * @return TransactionResource
     */
    public function get(Request $request, $transactionId)
    {
        $response = $this->service->get($transactionId);

        return new TransactionResource($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function validation(Request $request)
    {
        $response = $this->service->authorize($request->all());

        return new JsonResponse($response, $response['code']);
    }

}