<?php

namespace App\Http\Controllers;

use App\Http\Resources\SellerResource;
use App\Services\SellerService;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    private $service;

    public function __construct(SellerService $sellerService)
    {
        $this->service = $sellerService;
    }

    /**
     * @param Request $request
     * @return SellerResource
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'cnpj' => 'bail|required|size:14|unique:sellers',
            'fantasy_name' => 'bail|required|string',
            'social_name' => 'bail|required|string',
            'user_id' => 'bail|required|unique:sellers|integer',
            'username' => 'bail|required|unique:sellers|unique:consumers'
        ]);

        $seller = $this->service->create($request->all());

        return new SellerResource($seller);
    }
}