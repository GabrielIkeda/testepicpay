<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Http\Request;

class SellerResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        self::withoutWrapping();
        return [
            'cnpj' => $this->cnpj,
            'fantasy_name' => $this->fantasy_name,
            'id' => $this->id,
            'social_name' => $this->social_name,
            'user_id' => $this->user_id,
            'username' => $this->username
        ];
    }
}