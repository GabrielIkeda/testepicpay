<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Http\Request;

class AccountResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        self::withoutWrapping();
        return [
            'id' => $this->id,
            'owner_id' => $this->owner_id,
            'user_id' => $this->user_id,
            'type' => $this->type
        ];
    }
}