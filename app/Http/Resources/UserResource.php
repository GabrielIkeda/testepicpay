<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Http\Request;

class UserResource extends JsonResource
{
    protected $preserveKeys = true;

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        self::withoutWrapping();

        if (isset($this->consumer) || isset($this->seller)) {
            return [
                'cpf' => $this->cpf,
                'email' => $this->email,
                'full_name' => $this->full_name,
                'id' => $this->id,
                'password' => $this->password,
                'phone_number' => $this->phone_number,
                'accounts' => [
                    'consumer' => new ConsumerResource($this->consumer) ?? null,
                    'seller' => new SellerResource($this->seller) ?? null
                ]
            ];
        }

        return [
            'cpf' => $this->cpf,
            'email' => $this->email,
            'full_name' => $this->full_name,
            'id' => $this->id,
            'password' => $this->password,
            'phone_number' => $this->phone_number
        ];
    }
}   