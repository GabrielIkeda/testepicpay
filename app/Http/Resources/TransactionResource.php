<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id' => $this->id,
            'payee_id' => $this->payee_id,
            'payer_id' => $this->payer_id,
            'transaction_date' => $this->transaction_date,
            'value' => $this->value
        ];
    }

}