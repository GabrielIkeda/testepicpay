<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Http\Request;

class ConsumerResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        self::withoutWrapping();
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username
        ];
    }
}