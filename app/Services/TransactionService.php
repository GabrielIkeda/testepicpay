<?php

namespace App\Services;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TransactionService
{
    private $repository;

    /**
     * TransactionService constructor.
     * @param TransactionRepository $transaction
     */
    public function __construct(TransactionRepository $transaction)
    {
        $this->repository = $transaction;
    }

    public function get(int $transactionId)
    {
        $transaction = Transaction::find($transactionId);

        if (empty($transaction)) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => 'Transacao nao encontrada'
                ];
        }

        return $transaction;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function create(array $parameters)
    {
        try {
            $request = Request::create(Transaction::URL_AUTHORIZE_TRANSACTION, 'POST', $parameters);
            $response = app()->handle($request);

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                $response = json_decode($response->getContent(), true);

                return [
                    'code' => $response['code'],
                    'message' => $response['message']
                ];
            }

            return $this->repository->create($parameters);

        } catch (\Exception $e) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function authorize(array $parameters)
    {
        if ($parameters['value'] >= Transaction::MIN_VALUE_TRANSACTION) {
            return [
                'code' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Transacao nao autorizada'
            ];
        }

        return [
            'code' => Response::HTTP_OK,
            'message' => 'Autorizado'
        ];
    }
}