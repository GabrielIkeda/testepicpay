<?php

namespace App\Services;

use App\Models\Account;

class AccountService
{

    /**
     * @param array $parameters
     * @return Account
     */
    public static function create(array $parameters):Account
    {
        return Account::create($parameters);
    }

}