<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;
use App\Repositories\SellerRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class SellerService
{
    private $repository;

    /**
     * SellerService constructor.
     * @param SellerRepository $sellerRepository
     */
    public function __construct(SellerRepository $sellerRepository)
    {
        $this->repository = $sellerRepository;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function create(array $parameters)
    {
        $user = User::find($parameters['user_id']);

        if (empty($user)) {
            abort(Response::HTTP_NOT_FOUND, 'Usuário nao encontrado');
        }

        try {
            DB::beginTransaction();

            $seller = $this->repository->create($parameters);

            $accountParameters = [
                'owner_id' => $seller->id,
                'type' => Account::TYPE_SELLER,
                'user_id' => $seller->user_id
            ];

            AccountService::create($accountParameters);

            DB::commit();

            return $seller;
        } catch (\Exception $e) {
            DB::rollBack();
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }
}