<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;

class UserService
{

    private $repository;

    public function __construct(UserRepository $userRepository)
    {
        $this->repository = $userRepository;
    }

    /**
     * @param array $parameters
     * @return Collection
     * @throws \Exception
     */
    public function list(array $parameters):?Collection
    {
        if (!isset($parameters['q'])) {
            return $this->repository->getAll();
        }

        return $this->repository->getByNameOrUsername($parameters['q']);
    }

    /**
     * @param array $parameters
     * @return User
     */
    public function create(array $parameters):User
    {
        return $this->repository->create($parameters);
    }

    /**
     * @param $userId
     * @return User
     * @throws \Exception
     */
    public function get($userId):User
    {
        $user = $this->repository->getWithRelations($userId);

        if (empty($user)) {
            throw new \Exception('Usuario nao encontrado', Response::HTTP_NOT_FOUND);
        }

        return $user;
    }
}