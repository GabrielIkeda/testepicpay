<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;
use App\Repositories\ConsumerRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ConsumerService
{

    private $repository;

    public function __construct(ConsumerRepository $consumerRepository)
    {
        $this->repository = $consumerRepository;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function create(array $parameters)
    {
        $user = User::find($parameters['user_id']);

        if (empty($user)) {
            abort(Response::HTTP_NOT_FOUND, 'Usuário nao encontrado');
        }

        try {
            DB::beginTransaction();

            $consumer = $this->repository->create($parameters);

            $accountParameter = [
                'owner_id' => $consumer->id,
                'type' => Account::TYPE_CONSUMER,
                'user_id' => $consumer->user_id
            ];

            AccountService::create($accountParameter);

            DB::commit();

            return $consumer;
        } catch (\Exception $e) {
            DB::rollBack();
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }
}