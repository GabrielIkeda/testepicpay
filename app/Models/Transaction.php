<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    public const URL_AUTHORIZE_TRANSACTION = 'http://localhost:8000/validation';
    public const MIN_VALUE_TRANSACTION = 100;

    /**
     * @var array
     */
    protected $fillable = [
        'payee_id', 'payer_id', 'transaction_date', 'value'
    ];

    public $timestamps = false;
}