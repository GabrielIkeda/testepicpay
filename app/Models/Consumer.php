<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{

    protected $table = 'consumers';

    /**
     * @var array
     */
    protected $fillable = [
        'username', 'user_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'owner_id');
    }
}