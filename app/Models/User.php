<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = [
        'full_name', 'cpf', 'email', 'password', 'phone_number'
    ];

    public $timestamps = false;

    public function seller()
    {
        return $this->hasOne(Seller::class);
    }

    public function consumer()
    {
        return $this->hasOne(Consumer::class);
    }

    public function account()
    {
        return $this->hasMany(Account::class);
    }
}