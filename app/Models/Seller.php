<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'social_name', 'fantasy_name', 'cnpj', 'username', 'user_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'owner_id');
    }
}