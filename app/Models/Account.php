<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public const TYPE_CONSUMER = 'CONSUMER';
    public const TYPE_SELLER = 'SELLER';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'owner_id', 'user_id'
    ];

    public $timestamps = false;

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'owner_id');
    }

    public function consumer()
    {
        return $this->belongsTo(Consumer::class, 'owner_id');
    }
}