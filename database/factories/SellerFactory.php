<?php

use App\Models\Seller;
use Faker\Generator as Faker;

$factory->define(Seller::class, function (Faker $faker) use ($factory) {
   return [
       'cnpj' => $faker->unique()->numerify('##############'),
       'fantasy_name' => $faker->name,
       'social_name' => $faker->name,
       'username' => $faker->userName
   ];
});