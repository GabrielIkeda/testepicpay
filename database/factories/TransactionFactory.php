<?php

use App\Models\Transaction;
use Carbon\Carbon;
use Faker\Generator as Faker;
use \Illuminate\Support\Facades\DB;

$factory->define(Transaction::class, function (Faker $faker) {

    $payee_id = DB::table('sellers')->max('id');
    $payer_id = DB::table('consumers')->max('id');

    return [
        'payee_id' => $payee_id,
        'payer_id' => $payer_id,
        'transaction_date' => Carbon::now('america/sao_paulo')->format('Y-m-d\TH:i:s.u'),
        'value' => $faker->randomNumber(2)
    ];
});