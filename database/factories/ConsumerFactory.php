<?php

use App\Models\Consumer;
use Faker\Generator as Faker;

$factory->define(Consumer::class, function (Faker $faker) {
    return [
        'username' => $faker->userName
    ];
});
