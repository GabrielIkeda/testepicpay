<?php

use App\Models\Transaction;
use \Symfony\Component\HttpFoundation\Response;

class TransactionTest extends TestCase
{
    public function testSuccessCreation()
    {
        $transaction = factory(Transaction::class)->make()->toArray();
        $this->json('POST', '/transactions', $transaction);
        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testErrorCreation()
    {
        $transaction = factory(Transaction::class)->make()->toArray();
        unset($transaction['value']);
        $this->json('POST', '/transactions', $transaction);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testNotAuthorize()
    {
        $transaction = factory(Transaction::class)->make(['value' => random_int(100, 200)])->toArray();
        $this->json('POST', '/validation', $transaction);
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

}