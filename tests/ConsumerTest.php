<?php

use App\Models\Consumer;
use \App\Models\User;
use \Symfony\Component\HttpFoundation\Response;

class ConsumerTest extends TestCase
{

    public function testSuccessCreation()
    {
        $user = factory(User::class)->create()->id;

        $consumer = factory(Consumer::class)->make([
            'user_id' => $user
        ]);
        $this->json('POST', '/users/consumers', $consumer->toArray());
        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testErrorCreation()
    {
        $consumer = factory(Consumer::class)->make()->toArray();

        $this->json('POST', '/users/consumers', $consumer);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}