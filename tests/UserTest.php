<?php

use App\Models\User;
use \Symfony\Component\HttpFoundation\Response;

class UserTest extends TestCase
{

    public function testSuccessCreation()
    {
        $user = factory(User::class)->make();
        $this->json('POST', '/users', $user->toArray());
        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testErrorCreation()
    {
        $user = factory(User::class)->make()->toArray();
        unset($user['full_name']);
        $this->json('POST', '/users', $user);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }



}