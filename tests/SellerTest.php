<?php

use App\Models\Seller;
use \App\Models\User;
use \Symfony\Component\HttpFoundation\Response;

class SellerTest extends TestCase
{

    public function testSuccessCreation()
    {
        $user = factory(User::class)->create()->id;

        $consumer = factory(Seller::class)->make([
            'user_id' => $user
        ]);
        $this->json('POST', '/users/sellers', $consumer->toArray());
        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testErrorCreation()
    {
        $consumer = factory(Seller::class)->make()->toArray();

        $this->json('POST', '/users/sellers', $consumer);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}